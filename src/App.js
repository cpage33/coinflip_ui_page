import React, { Component } from "react";
import { getFlips, makeFlipCall } from "./coinSevice";
import { Button, Alert, Input } from "reactstrap";
import CoinComponent from "./CoinComponent";

import "./App.css";

/**
 * Main component that we actually touch
 */
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flips: 1,
      outcome: "",
      side: "",
      coinImage: "",
      outcomePercentage: "",
      otherOutcomes: [],
      isLoaded: true
    };

    //making sure that these functions can interact with state
    this.setFlips = this.setFlips.bind(this);
    this.getCoinFlips = this.getCoinFlips.bind(this);
  }

  /**
   * makes api call to coinFlip and sets the outcomes
   */
  getCoinFlips() {
    getFlips(this.state.flips).then(outcome => {
      this.setState({
        outcome: outcome.outcome,
        coinImage: outcome.coinImage,
        outcomePercentage: outcome.outcomePercentage,
        otherOutcomes: outcome.otherOutcomes
      });
    });
  }

  /**
   * called by input on change.  Sets the is
   * @param {event handler} event
   */
  setFlips(event) {
    this.setState({ flips: event.target.value });
  }

  render() {
    let alert =
      this.state.flips < 1 ||
      this.state.flips % 2 == 0 ||
      this.state.flips > 29 ? (
        <Alert color="danger">
          Please enter an even number greater than 0 but less than 29.
        </Alert>
      ) : (
        ""
      );
    console.log(this.state);
    let flipOutput = this.state.isLoaded ? (
      <CoinComponent
        outcome={this.state.outcome}
        coinImage={this.state.coinImage}
        outcomePercentage={this.state.outcomePercentage}
        otherOutcomes={this.state.otherOutcomes}
      />
    ) : (
      ""
    );

    return (
      <div className="App">
        <header className="App-header">
          <p>Welcome to Conner's CoinFlip API!</p>
          <Input
            className="num-input"
            onChange={this.setFlips}
            placeholder="Number of Flips"
            type="number"
            max="29"
            min="1"
          />

          <Button className="query-button" onClick={this.getCoinFlips}>
            Flip Coin(s)
          </Button>
          {alert}
          {flipOutput}
        </header>
      </div>
    );
  }
}

export default App;
