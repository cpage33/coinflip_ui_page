import React, { Component } from "react";
import "./App.css";

export default class CoinComponent extends Component {
  constructor(props) {
    super(props); //constructor & super calls w/ props argument are required for props to work
  }

  render() {
    //   return multiline jsx objects in parentheses
    // must have one parent / object in your return
    let arrFlips = [];
    this.props.otherOutcomes.forEach(flip => {
      arrFlips.push(
        <div>
          <img className="outcomeImg" src={flip.coinImage} />
        </div>
      );
    });
    return (
      <div>
        <div>outcome: {this.props.outcome}</div>
        <div>
          coinImage: <img className="coinImage" src={this.props.coinImage} />
        </div>
        <div>outcomePercentage: {this.props.outcomePercentage}</div>
        <div>allOutcomes:{arrFlips}</div>
      </div>
    );
  }
}
