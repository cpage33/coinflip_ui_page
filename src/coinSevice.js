import rp from "request-promise";

export function getFlips(flips) {
  return rp({
    method: "GET",
    uri: "https://cpage-coin-flip-api.herokuapp.com/coinflip/" + flips,
    json: true
  });
}

export function makeFlipCall(uri) {
  return rp({
    method: "GET",
    uri: uri,
    json: true
  });
}
